require "spec"
require "../src/http/echo/server/handler"
  
  #TODO add tests  

  describe "EchoHandler::Object" do
    it "returns expected JSON" do
      handler = EchoHandler.new
      req = HTTP::Request.new("GET", "/mypath")

      json = handler.get_request_json(req)
      json.should_not be_nil
      json.should eq "{\"host\":null,\"method\":\"GET\",\"path\":\"/mypath\",\"query_params\":{},\"headers\":{}}"

    end
  end