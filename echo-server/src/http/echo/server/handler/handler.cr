require "http/server"
require "json"

# EchoHandler - a handler that handles an HTTP request by echoing it as a JSON object response
class EchoHandler
  
  # sets the HTTP response as a JSON object of the request
  def echo(ctx : HTTP::Server::Context)
    ctx.response.content_type = "application/json"
    
    json = get_request_json(ctx.request)

    ctx.response.print json
  end

  # build response JSON from request
  def get_request_json(req : HTTP::Request) : String
    resJson = JSON.build do |json|
      json.object do 
        json.field "host", req.host
        json.field "method", req.method
        json.field "path", req.path
        
        # nested query_params
        query_params = get_query_params(req)
        json.field "query_params", query_params
          
        # nested http headers
        headers = get_headers(req)
        json.field "headers", headers
    
        # request body
        body_string = req.body.try(&.gets_to_end)
        if body_string
          json.field "body", body_string
        end
      end
    end
    return resJson
  end

  # returns the HTTP request's query params as a Hash object from the context
  def get_query_params(req : HTTP::Request) : Hash
    params = {} of String => String
    req.query_params.each do |name, value|
      params[name] = value
    end
    return params
  end


  # get the HTTP headers as a Hash object from the context
  def get_headers(req : HTTP::Request) : Hash
    headers = {} of String => String
    req.headers.each do |name, value|
      headers[name] = value[0]  
    end    
    return headers  
  end

end
