require "./handler"

# A simple HTTP server that replies with an html/json response describing the received request (headers, path, host, body, etc.):
# Example: { host: 'lambda.aws.....', headers: { "Content-Type": "application/json" }, body: "", ... }
module EchoServer
    VERSION = {{ `shards version #{__DIR__}`.chomp.stringify }}
  
    # Handle Ctrl+C and kill signal.
    # Needed for hosting this process in a docker
    # as the entry point command
    Signal::INT.trap { puts "Caught Ctrl+C..."; exit }
    Signal::TERM.trap { puts "Caught kill..."; exit }
  
    host = "0.0.0.0"
    port = 80

    handler = EchoHandler.new
    
    # start an http server with the default handlers for errors and logs 
    server = HTTP::Server.new([HTTP::ErrorHandler.new, HTTP::LogHandler.new]) do |ctx|
      handler.echo(ctx)
    end

    address = server.bind_tcp(host, port)
    puts "Listening on http://#{address}"
    server.listen

end